// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DetectiveGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DETECTIVE_API ADetectiveGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
